# EucFACE: storage of non-structural carbohydrates

This repository contains the code to analyze the starch + sugar data collected at the EucFACE in late 2015. The analysis is based on the files : 

- `FACE_P0078_RA_WOODCARB_L1_20151101-20151201.csv`
- `FACE_P0078_RA_BARKCARB_L1_20151115-20151215.csv`
- `FACE_P0078_RA_LEAFCARB_L1_20151115-20151215.csv`

You must have access to the HIEv to run this analysis, and set the API token the usual way, or see `load.R`. 

To make the figures do:

```
source("load.R")
source("make_figures.R")
```

which produces PDFs in `/output`.
